import { createStore } from 'vuex';
import teachersModule from './teachersModule';
import userModule from './modules/user';


export default createStore({
 
  state: {
    lessons: [
      { id: 1, name: 'Математика' },
      { id: 2, name: 'Науки' },
      { id: 3, name: 'Англійська' },
      { id: 4, name: 'Програмування' },
    ],
    selectedTeachers: {},
  },
  getters: {
    lessonsGetter: (state) => state.lessons,
    lessonByIdGetter: (state) => (lessonId) => {
      return state.lessons.find((lesson) => lesson.id === parseInt(lessonId));
    },
    selectedTeachersGetter: (state) => state.selectedTeachers,
  },
  mutations: {
    updateSelectedTeacher(state, { lessonId, teacherId }) {
      state.selectedTeachers = {
        ...state.selectedTeachers,
        [lessonId]: teacherId,
      };
    },
  },
  actions: {
    updateSelectedTeacher({ commit }, { lessonId, teacherId }) {
      commit('updateSelectedTeacher', { lessonId, teacherId });
    },
  },
  modules: {
    teachers: teachersModule, 
    user: userModule,
   
  },
});