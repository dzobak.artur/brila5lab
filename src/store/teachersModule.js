export default {
    state: {
      teachers: [
        { id: 1, name: 'Брила' },
        { id: 2, name: 'Вогар' },
        { id: 3, name: 'Андрашко' },
        { id: 4, name: 'Ромочівська' },
      ],
    },
    getters: {
      teachersGetter: (state) => state.teachers,
    },
  };